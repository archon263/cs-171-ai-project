#include "AIShell.h"
#include <iostream>
#include <cstdlib>
#include "stdio.h"
#include "string.h"
// #include "limits.h"
#include <queue>	//used in makeMove, tempAction, and others to get a set of all possible moves to make
#include <sys/time.h>
#include <time.h>
#include <chrono>

typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::system_clock sClock;

/*
	Hello viewers, this is the current working commit. 
	Thing I know to be somewhat wrong here.
	
	Heurisitc is almost working,

	Now i am doing alpha beta

 */
bool TAKENEXT = false;	//take next move in case of win
#define MAX(X,Y) (((X) > (Y)) ? (X) : (Y))
#define MIN(X,Y) (((X) < (Y)) ? (X) : (Y))

<<<<<<< HEAD:AlphaBeta_AI/src/AIShell.cpp
#define Hmax 100000
#define Hmin -100000
//try module load java/1.8.0_20 instead of 1.8_20
=======
#define Hmax INT_MAX
#define Hmin INT_MIN
#define scoreCut 50000	//cutoff score for IDS
#define convertM 1000

clock_t lastClock = clock();
int searchCutoff = 0;
//try module load java/1.8.0_20 
>>>>>>> d1de000baa1c981c162ab721cd183e7fe189b33a:IDS_AI/AIShell.cpp
namespace offSet {
	static int x[] = {0, -1, -1, -1, 0, 1, 1, 1};	//count row index around center
	static int y[] = {-1, -1, 0, 1, 1, 1, 0, -1};	//count col index around center
	/*
		Checks index value around s in <^>v fashion, starting at x, ending at o
		|->	->	v
		|x	s 	v
		|o	<-	<-
	*/
}

using namespace std;

void AIShell::pout (int **arrays)
{
	std::cout<<"Time to move again\n";
 	for ( int i = numRows-1 ; i>=0; i--)
		{
			for (int j = 0 ; j<numCols; j++)
			{
				char ply ;
				int what = arrays[j][i];
				if (what == -1)
					ply = 'x';
				else if (what == 0)
					ply = '-';
				else
					ply = 'o';
				std::cout<<ply<<" ";
			}
			std::cout<<'\n';
		}
}

AIShell::AIShell(int numCols, int numRows, bool gravityOn, int** gameState, Move lastMove)
{
	this->deadline=0;
	this->numRows=numRows;
	this->numCols=numCols;
	this->gravityOn=gravityOn;
	this->gameState=gameState;
	this->lastMove=lastMove;
}


AIShell::~AIShell()
{
	
	//delete the gameState variable.
	for (int i =0; i<numCols; i++){ 
		delete [] gameState[i];
	}
	delete [] gameState;

}

void remove(int **gState, Move m)
{
	gState[m.col][m.row] = 0;
}

Move AIShell::makeMove(){
	int **gState = gameState; 
 	clock_t start = clock(), test = clock();	//clock that I wil be using to figure out time per case
	int alpha = Hmin,  beta = Hmax;

	Move m = Move();
	int depth = 1;	//start at 1 for the ply beneath zero.
	std::queue<Move> allMoves = tempAction(gState);
	// cout<<"allMoves size : "<<allMoves.size()<<endl;
	// pout (gState);
	int bestVal = Hmin;
<<<<<<< HEAD:AlphaBeta_AI/src/AIShell.cpp
	//for each state, apply action
	//This is the base state, we are starting to figure out trees of the first ply

	//Problem, it need to have some way to do the last move regardless

=======
>>>>>>> d1de000baa1c981c162ab721cd183e7fe189b33a:IDS_AI/AIShell.cpp
	int v = Hmin;
	start = clock() - lastClock;	//this is a clock struct man

	int TimePerMove = deadline/allMoves.size();	//this is how much time each move gets for eval
	
	for (Move i = allMoves.front(); allMoves.size() > 0 ; allMoves.pop())
	{
<<<<<<< HEAD:AlphaBeta_AI/src/AIShell.cpp
		// cout<<"This many moves left "<<allMoves.size()<<endl;
	
		//first, figure out what computers next move will be, then minimax down
		i = allMoves.front();	//This is a Move
		//added max here
		// cout << "\n Going to move " << i.col << i.row << "is " << bestVal << endl;
		bestVal = alphaBeta(tempApply(gState,i,AI_PIECE), depth, HUMAN_PIECE, alpha, beta );	//do ai move now
		remove(gState,i);
		// cout<<"Here's our bestVal "<<bestVal<<std::endl;
=======
		i = allMoves.front();	//This is a Move

		start = clock() -lastClock;
		bestVal = IDS_search(tempApply(gState,i,AI_PIECE), start, TimePerMove);

		// bestVal = alphaBeta(tempApply(gState,i,AI_PIECE), depth, HUMAN_PIECE,
		//  alpha, beta, start);	//do ai move now
		remove(gState,i);
		// cout<<"Here's our bestVal "<<bestVal<<std::endl;
		// cout << "\nBestVal for " << i.col << i.row << "is " << bestVal << endl;
		
		start = clock() - lastClock;
		lastClock = clock();	//solves the problem of reseting the clock.
>>>>>>> d1de000baa1c981c162ab721cd183e7fe189b33a:IDS_AI/AIShell.cpp

		if (bestVal >v)
		{
			m.row= i.row;	//switch one move to the best move
			m.col = i.col;
			v = bestVal;
 		}
 		if (v>= beta )
 			break;
 		if (v >alpha)
 			alpha = v;



	}
	// test = clock() - test;
	// cout<<"here's test time : " <<test/convertM<<endl;
	// start = clock() - lastClock;
	// lastClock = clock();	//solves the problem of reseting the clock.

	// cout<<"We will use these values "<<m.col<<m.row<<std::endl;
	// cout<<"Here is the time it took to do this "<<((long)start)/convertM<<endl;	

	return m;
}

int AIShell::validMove ( int col, int row)
{	//Check if indexes are within boundary and if gState is empty at those indexes
	if (!(row>-1 and row <numRows and col >-1 and col <numCols))
		return false;
	return true;
}

int AIShell::HeuristicEval ( int **gState, int player)
{ 
/*	This function will determine what will the best move to make for a player,
	this will be used inside minimax. Will use win checker to see utility of a certain gameboard for a particular player
	return min val for human (p1)
	return max val for AI  (p2)
	cout<<" I am in HeuristicEval"<<std::endl;
*/
	int val = 0;
	val += countAllrows(gState);
	return val;
 };

int giveWinPoints(int a, int h, int e, int k)
{
	int ai = a + e , hum = h + e;
	if (a>= k)
		return 100000;
	else if (ai >= k)
		return 1;
	
	if (h >= k)
		return 100000;
	else if (hum>= k)
		return 1;

	return 0;
}

void changeCounts(int current, int player, int &A, int &H, int &E)
{
	if (player == -1)	//Human piece
	{
		if (current == -1)
			H++;
		else if (current == 1)
			H = 0;
		else
			E++;
	}
	else
	{
		if (current == -1)
			A = 0;
		else if (current == 1)	//AI piece
			A++;
		else
			E++;
	}

}

int AIShell::countDir(int **gState, int col, int row, int player, int direction)
{
	//First calculate the number of winning spaces, then returns a win point of 1 if a win could be found. 		OR +INF if a win IS FOUND
	int A = 0, H = 0 , E = 0;
	int currentCell = 0;
	int count = 0;
	currentCell = gState[col][row];
	while (count <k)	//0 ,1 ,2 ,3 ,4
	{
		switch (direction)
		{
			case 1:
				if (direction == 1 &&  validMove(col+count, row))
					currentCell = gState[col+count][row];
				else
					return 0;

			case 2:
				if (direction == 2 &&  validMove(col, row+count))
					currentCell = gState[col][row+count];
				else 
					return 0;

			case 3:
				if (direction == 3 &&  validMove(col+count, row+count))
					currentCell = gState[col+count][row+count];
				else 
					return 0;
			case 4:
				if (direction == 4 &&  validMove(col+count, row-count))
					currentCell = gState[col+count][row-count];
				else 
					return 0;
		}

		

		changeCounts (currentCell, player, A, H ,E);	//increment piece tracker by one 
		// cout<<"What are my AHE?!?!? "<< A << H << E<<endl;
		count++;
	}

	return giveWinPoints(A, H , E, k);	//give 1, 0, or inf depending if there are k pieces in a row.
	


}

int AIShell::countAllrows(int **gState)
{
	/*
	Count number of current winning spaces a board has. A player is awared one point if the sum of their piece + empty pieces are = k
	ie K=4  A= 2, E = 2	--> 	A + E = 2+2 = 4		--> p1 += 1
	A win is considered when a player has achieve K amount of pieces without the addition of empty spaces
	*/

	int p1 = 0, p2 = 0;	//p1 = AI, p2 = Human
	int winner = winMove(gState);
	if (winner!= 0 )
	{
		pout(gState);
		cout<<"it exists!!!! "<<winner<<"\n\n";
		if (winner == -1)
			return -100000;
		else
			return 	100000;
	}

	for (int col = 0 ; col < numCols; col++)	//for each column
	{
		for (int row = 0; row < numRows; row++)	//for each row
		{
			// count in four directions
			// after each direction, immediately check if either a or h = k and return immedietly if found
			// otherwise, add together a + e and h + e and increment p1 or p2 as seen fit
			//count to the right
			//count up
			//count diag up
			//count diag down
			for (int i = 1 ; i <= 4; i++)
			{
				p1 += countDir(gState, col, row, 1, i);	//count for ai first
				p2 += countDir(gState, col, row, -1, i);	//count for human next
				// cout<<"P1: "<<p1<<" vs P2: "<<p2<<endl;
			}
			// cout<<"\nnext line "<< col << " " << row<<endl;
			
		}
		
	}
	// cout<<"it should hit this"<<endl;
	return (p1 - p2);
}

int AIShell::IDS_search(int ** gState, clock_t currentClock, int timeLimit)	//i can call deadline so I don't need to pass more chorno::clocks
{

	long currentTime = ((long)currentClock)/convertM;	//get the current remaining time in ms

	int depth = 1;
	int score = 0;
	searchCutoff = 0;

	while (true)
	{
		currentClock = clock() -lastClock ;
		currentTime = ((long)currentClock)/convertM;
		// cout<<"Here's the time: "<<currentTime<<endl;
		
		if (currentTime >= timeLimit)
			break;

		currentClock = clock() - lastClock;
		int result = alphaBeta(gState, depth, HUMAN_PIECE, Hmin, Hmax, currentClock, timeLimit);	//gstate is already modified

		// cout<<"result of : "<< result<<endl;
		if (result >= scoreCut)	//if winning is found, just return that value
			return result;
		if (!searchCutoff)
			score = result;
		depth++;
	}

	return score;
}


//Need to change this one
int AIShell::alphaBeta (int **gState, int ply, int player , int alpha, int beta,
  clock_t cTime, int timeLimit)
{
	//miniMax algorithm designede for 4 ply with branching equal to width
	//or 2/3 ply with the entire gameboard as the branch

//determine which player is it at the terminal state to get value
//Player 1(Computer 1) gets max val/ Player 2 (Human -1) gets min value
	int currentPly = ply;
	int win = winMove(gState);
	long currentT = ((long)cTime)/convertM ;

	if (currentT >= timeLimit)
		searchCutoff = 1;
	// cout<<"Current clock cycle: "<<currentT<<endl;

 	
	if (win != 0 )
	{
		// pout(gState);
		// cout<<"it exists!!!! "<<win<<"\n\n";
		if (win == -1)
			return -100000;
		else
			return 	100000;
	}
	// if (currentPly >=4)
	// 	cout<<"it is 4th depth now"<<endl;
	// cout<<"current ply : "<<currentPly<<endl;

	if (currentPly == 0 || (currentT >= timeLimit) )	//cutoff now works
		return HeuristicEval(gState,player);
<<<<<<< HEAD:AlphaBeta_AI/src/AIShell.cpp
	// pout(gState);

	what++;	//this increments ply
 	// cout<<"what depth and move? "<<endl;
	// may need to not do max min for alpha beta part
=======
 	currentPly--;

  	// may need to not do max min for alpha beta part
>>>>>>> d1de000baa1c981c162ab721cd183e7fe189b33a:IDS_AI/AIShell.cpp
	if ( player == 1)	//is computer turn,move for computer
		{
			int v = Hmin;
			std::queue<Move> allMoves = tempAction(gState);
			if (allMoves.size() == 1)
				{
					return HeuristicEval(tempApply(gState, allMoves.front(), AI_PIECE) ,player);
					 // int val = HeuristicEval(tempApply(gState, allMoves.front(), AI_PIECE) ,player);
					 // remove(gState, allMoves.front());
					 // return val;
				}
			for (Move i = allMoves.front() ; allMoves.size() > 0; allMoves.pop())
			{
				cTime = clock() - lastClock;	//don't forget to clock in every call and minus previous call time
				
				i = allMoves.front();
				
				//find best value of human opponenet
				int bestVal = alphaBeta(tempApply(gState, i, AI_PIECE ),
					currentPly, -1,alpha,beta, cTime, timeLimit);	
				// std::cout<<"in ai move\n\n";
				remove(gState, i);	//need to remove the affected state for the other branches

				if (bestVal > v)
					v = bestVal;
				if (v >= beta)
					return v;
				if (v > alpha)
					alpha = v;
				// if (alpha >= beta)
				// 	return v;
			}
			
			// p2 = Clock::now();
			// cout<<"Stead test: "<<(p2 - p1 ).count()/1000000<<endl;
			// test2 = Clock::now();
			// cout<<"test is : "<<(test2- test1).count()/1000000<<endl;
			return v;

		}
	else if (player == -1)	//is human turn, move for humans
		{
			int v = Hmax;
			std::queue<Move> allMoves = tempAction(gState);
			if (allMoves.size() == 1)
			{
				return HeuristicEval(tempApply(gState, allMoves.front(), HUMAN_PIECE) ,player);
					 // int val = HeuristicEval(tempApply(gState, allMoves.front(), HUMAN_PIECE) ,player);
					 // remove(gState, allMoves.front());
					 // return val;
				}
			for (Move i = allMoves.front() ; allMoves.size() > 0; allMoves.pop())
			{
				cTime = clock() - lastClock;
				i = allMoves.front(); //find best value of human opponenet
 				
 				
				int bestVal = alphaBeta(tempApply(gState, i, HUMAN_PIECE ),currentPly, 1 ,
				 alpha, beta, cTime, timeLimit );
				// std::cout<<"in human move\n\n";
				remove(gState, i);

				if (bestVal < v)
					v = bestVal;
				if (v <= alpha )
					return v;
				if (v < beta )
					beta = v;
				// if (alpha >= beta)
				// 	return v;
			}
			return v;
		}
};

std::queue<Move> AIShell::tempAction(int **gState)	//Makes a temp **int to gamestate, applies action without affecting current board
{	//Need to store all possible states
	std::queue<Move> Movelist;
	for (int i = 0; i < numCols; i++){
		for (int j = 0; j < numRows; j++){
			if (gState[i][j] == NO_PIECE)
			{
				Move m = Move(i,j);
				Movelist.push(m);
				if (gravityOn)
					break;
			}
		}
	}
	return Movelist;
};

int **AIShell::tempApply (int **gState, Move m, int ply)
{
	//return a temp array that gets destroyed after all function calls in minimax and makeMove
	int ** temp = gState;
	if (ply == HUMAN_PIECE)
		temp[m.col][m.row] = HUMAN_PIECE; // -1
	else
		temp[m.col][m.row] = AI_PIECE; // 1
	return temp;
}


bool AIShell::cutOff (int ply )
{
	if (gravityOn)
	{	
		if (ply == 4)
				return true;
	}
	else 
	{
		if (ply == 4)
			return true;
	}
	return false;
}


int AIShell::winCounter( int **gState)
{	
int winner = 0;
for (int i =0 ; i < numCols; i++)
	{
		for (int j =0 ; j <numRows; j++)
		{
			// std::cout<<"i and j "<< i<<j<<"\n";
			if (validMove(i, j+1 ) && gState[i][j] != 0)	//count vertically, col-row wise
			{
				// std::cout<<"FIRST \n";
				int count = 0;
				if (!validMove(i, j+count))
						break;
				do
				{
					if (gState[i][j+count] == gState[i][j])	//ONLY INCREMENT IF YOU HAVE MATCH
						count++;
					if (count >= k)
					{
						winner = gState[i][j];
						return winner;
					}
					if (!validMove(i, j+count))
						break;
				}
				while (gState[i][j] == gState[i][j+count]);

			}
			if (validMove( i+1, j )&& gState[i][j] != 0)	//count horizontally
			{
				// std::cout<<"SECOND \n";
				int count = 0;
				if (!(validMove(i+count, j)))
						break;
				do
				{
					if (gState[i+count][j] == gState[i][j])	//ONLY INCREMENT IF YOU HAVE MATCH
						count++;
					if (count >=k)
					{
						winner = gState[i][j];
						return winner;
					}
					if (!(validMove(i+count, j)))
						break;
				}
				while (gState[i][j] == gState[i+count][j]);
				
			}
			if (validMove(  i+1, j+1 ) && gState[i][j] != 0)	//count diagonally top left to bot right
				{
					// std::cout<<"THIRD \n";
					int count = 0;
					if (!validMove(i+count, j+count))
						break;
					do
					{
						// i+count = 7
						if (gState[i+count][j+count] == gState[i][j])	//ONLY INCREMENT IF YOU HAVE MATCH
							count++;
						if (count >=k)
						{
							winner = gState[i][j];
							return winner;
						}
						if (!validMove(i+count, j+count))
							break;
					}
					while(gState[i][j] == gState[i+count][j+count]); // 8 
					
				}
			if (validMove(  i+1, j-1 ) && gState[i][j] != 0)
			{
				// std::cout<<"fourth\n";
				int count = 0;
				if (!validMove(i +count, j -count))
						break;
				do
				{
					
					if (gState[i+count][j-count] == gState[i][j])	//ONLY INCREMENT IF YOU HAVE MATCH
						count++;
					if( count >= k)
					{
						winner = gState[i][j];
						return winner;
					}
					if (!validMove(i +count, j -count))
						break;
				}
				while(gState[i][j] == gState[i + count][j - count]);
				
			}
		}
	}
	return 0;
};

int AIShell::winMove (int **gState)
{
	int val = winCounter(gState);

	if (val == HUMAN_PIECE)
		return -1;
	else if (val == AI_PIECE)
		return 1;
	return 0;
}
int AIShell::countConnect(int **gState) // Counts the number of connected lines
{
	int p1Count = 0, p2Count = 0;

						/*
	static int x[] = {0, -1, -1, -1, 0, 1, 1, 1};	//count row index around center
	static int y[] = {-1, -1, 0, 1, 1, 1, 0, -1};	//count col index around center
*/
						/*
		Checks index value around s in <^>v fashion, starting at x, ending at o
		|->	->	v
		|x	s 	v
		|o	<-	<-
	*/

	// cout <<"Here a count of p1 vs p2:  "<<p1Count<< "   " << p2Count<<endl;
	return p1Count - p1Count;	//all values are positive counts
};
//win move isn't calculating win correctly
//also heuristic is incorrect




