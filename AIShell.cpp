#include "AIShell.h"
#include <iostream>
#include <cstdlib>
#include "stdio.h"
#include "string.h"
// #include "limits.h"
#include <queue>	//used in makeMove, tempAction, and others to get a set of all possible moves to make


/*
	Hello viewers, this is the current working commit. 
	Thing I know to be somewhat wrong here.
	
	Heurisitc is almost working,

	Now i am doing alpha beta

 */
bool TAKENEXT = false;	//take next move in case of win
#define MAX(X,Y) (((X) > (Y)) ? (X) : (Y))
#define MIN(X,Y) (((X) < (Y)) ? (X) : (Y))

#define Hmax 10000
#define Hmin -10000
//try module load java/1.8.0_20 instead of 1.8_20
namespace offSet {
	static int x[] = {0, -1, -1, -1, 0, 1, 1, 1};	//count row index around center
	static int y[] = {-1, -1, 0, 1, 1, 1, 0, -1};	//count col index around center
	/*
		Checks index value around s in <^>v fashion, starting at x, ending at o
		|->	->	v
		|x	s 	v
		|o	<-	<-
	*/
}

using namespace std;

void AIShell::pout (int **arrays)
{
	std::cout<<"Time to move again\n";
 	for ( int i = numRows-1 ; i>=0; i--)
		{
			for (int j = 0 ; j<numCols; j++)
			{
				char ply ;
				int what = arrays[j][i];
				if (what == -1)
					ply = 'x';
				else if (what == 0)
					ply = '-';
				else
					ply = 'o';
				std::cout<<ply<<" ";
			}
			std::cout<<'\n';
		}
}

AIShell::AIShell(int numCols, int numRows, bool gravityOn, int** gameState, Move lastMove)
{
	this->deadline=0;
	this->numRows=numRows;
	this->numCols=numCols;
	this->gravityOn=gravityOn;
	this->gameState=gameState;
	this->lastMove=lastMove;
	this->moveExists = true;
	this->winFound = false;
}


AIShell::~AIShell()
{
	
	//delete the gameState variable.
	for (int i =0; i<numCols; i++){ 
		delete [] gameState[i];
	}
	delete [] gameState;

}

void remove(int **gState, Move m)
{
	gState[m.col][m.row] = 0;
}

Move AIShell::makeMove(){
	//this part should be filled in by the student to implement the AI
	//Example of a move could be: Move move(1, 2); //this will make a move at col 1, row 2
	
	int **gState = gameState; 
 
	int alpha = Hmin;
	int beta = Hmax;

	Move m = Move();
	int depth = 1;	//start at 1 for the ply beneath zero.

	std::queue<Move> allMoves = tempAction(gState);

	// cout<<"allMoves size : "<<allMoves.size()<<endl;
	// pout (gState);
	int bestVal = Hmin;
	//for each state, apply action
	//This is the base state, we are starting to figure out trees of the first ply

	//Problem, it need to have some way to do the last move regardless

	int v = Hmin;
	for (Move i = allMoves.front(); allMoves.size() > 0 ; allMoves.pop())
	{
		// cout<<"This many moves left "<<allMoves.size()<<endl;
	
		//first, figure out what computers next move will be, then minimax down
		i = allMoves.front();	//This is a Move
		//added max here
		// cout << "\n Going to be using " << i.col << i.row << "is " << bestVal << endl;
		bestVal = alphaBeta(tempApply(gState,i,AI_PIECE), depth, HUMAN_PIECE, alpha, beta );	//do ai move now
		remove(gState,i);

		// cout<<"Here is our bestVal "<<bestVal<<std::endl;
		// if (bestVal <Hmin)
		// 	TAKENEXT = true;

		// if (TAKENEXT && bestVal>Hmin)	//if take next is true and best val is better than threshold, take it now to avoid loss.
		// {
		// 	TAKENEXT = false;
		// 	m = i;
		// 	return m;
		// }

		if (bestVal > v)
		{
			m.row= i.row;	//switch one move to the best move
			m.col = i.col;
			v = bestVal;
 		}
 		if (v>= (beta ))
 			break;
 		if (v >(alpha))
 			alpha = v;

	}
	// cout<<"We will use these values "<<m.col<<m.row<<std::endl;
	return m;
}

int AIShell::validMove ( int col, int row)
{	//Check if indexes are within boundary and if gState is empty at those indexes
	if (!(row>-1 and row <numRows and col >-1 and col <numCols))
		return false;
	return true;
}

int AIShell::HeuristicEval ( int **gState, int player)
{ 
/*	This function will determine what will the best move to make for a player,
	this will be used inside minimax. Will use win checker to see utility of a certain gameboard for a particular player
	return min val for human (p1)
	return max val for AI  (p2)
	cout<<" I am in HeuristicEval"<<std::endl;
*/
	int val = 0;
	val += countAllrows(gState);
	return val;
 };

int giveWinPoints(int a, int h, int e, int k)
{
	int ai = a + e , hum = h + e;
	if (a>= k)
		return 100000;
	else if (ai >= k)
		return 1;
	
	if (h >= k)
		return 100000;
	else if (hum>= k)
		return 1;

	return 0;
}

void changeCounts(int current, int player, int &A, int &H, int &E)
{
	if (player == -1)	//Human piece
	{
		if (current == -1)
			H++;
		else if (current == 1)
			H = 0;
		else
			E++;
	}
	else
	{
		if (current == -1)
			A = 0;
		else if (current == 1)	//AI piece
			A++;
		else
			E++;
	}

}

int AIShell::countDir(int **gState, int col, int row, int player, int direction)
{
	//First calculate the number of winning spaces, then returns a win point of 1 if a win could be found. 		OR +INF if a win IS FOUND
	int A = 0, H = 0 , E = 0;
	int currentCell = 0;
	int count = 0;
	currentCell = gState[col][row];
	while (count <k)	//0 ,1 ,2 ,3 ,4
	{
		switch (direction)
		{
			case 1:
				if (direction == 1 &&  validMove(col+count, row))
					currentCell = gState[col+count][row];
				else
					return 0;

			case 2:
				if (direction == 2 &&  validMove(col, row+count))
					currentCell = gState[col][row+count];
				else 
					return 0;

			case 3:
				if (direction == 3 &&  validMove(col+count, row+count))
					currentCell = gState[col+count][row+count];
				else 
					return 0;
			case 4:
				if (direction == 4 &&  validMove(col+count, row-count))
					currentCell = gState[col+count][row-count];
				else 
					return 0;
		}

		

		changeCounts (currentCell, player, A, H ,E);	//increment piece tracker by one 
		// cout<<"What are my AHE?!?!? "<< A << H << E<<endl;
		count++;
	}

	return giveWinPoints(A, H , E, k);	//give 1, 0, or inf depending if there are k pieces in a row.
	


}

int AIShell::countAllrows(int **gState)
{
	/*
	Count number of current winning spaces a board has. A player is awared one point if the sum of their piece + empty pieces are = k
	ie K=4  A= 2, E = 2	--> 	A + E = 2+2 = 4		--> p1 += 1
	A win is considered when a player has achieve K amount of pieces without the addition of empty spaces
	*/

	int p1 = 0, p2 = 0;	//p1 = AI, p2 = Human
	int winner = winMove(gState);
	if (winner!= 0 )
	{
		// pout(gState);
		// cout<<"it exists!!!! "<<win<<"\n\n";
		if (winner == -1)
			return -100000;
		else
			return 	100000;
	}

	for (int col = 0 ; col < numCols; col++)	//for each column
	{
		for (int row = 0; row < numRows; row++)	//for each row
		{
			// count in four directions
			// after each direction, immediately check if either a or h = k and return immedietly if found
			// otherwise, add together a + e and h + e and increment p1 or p2 as seen fit
			//count to the right
			//count up
			//count diag up
			//count diag down
			for (int i = 1 ; i <= 4; i++)
			{
				p1 += countDir(gState, col, row, 1, i);	//count for ai first
				p2 += countDir(gState, col, row, -1, i);	//count for human next
				// cout<<"P1: "<<p1<<" vs P2: "<<p2<<endl;
			}
			// cout<<"\nnext line "<< col << " " << row<<endl;
			
		}
		
	}
	// cout<<"it should hit this"<<endl;
	return (p1 - p2);
}


//Need to change this one
int AIShell::alphaBeta (int **gState, int ply, int player , int alpha, int beta )
{
	//miniMax algorithm designede for 4 ply with branching equal to width
	//or 2/3 ply with the entire gameboard as the branch

//determine which player is it at the terminal state to get value
//Player 1(Computer 1) gets max val/ Player 2 (Human -1) gets min value
	int what = ply;
	int win = winMove(gState);
	// cout << " win value " <<win<< endl;

	if (win != 0 )
	{
		// pout(gState);
		// cout<<"it exists!!!! "<<win<<"\n\n";
		if (win == -1)
			return -100000;
		else
			return 	100000;
	}

	if (cutOff(what) )	//cutoff now works
		return HeuristicEval(gState,player);
	pout(gState);

	what++;	//this increments ply
 	cout<<"what depth and move? "<<endl;
	// may need to not do max min for alpha beta part
	if ( player == 1)	//is computer turn,move for computer
		{
			int v = Hmin;
			std::queue<Move> allMoves = tempAction(gState);
			if (allMoves.size() == 1)
				{
					// return HeuristicEval(tempApply(gState, allMoves.front(), AI_PIECE) ,player);
					 int val = HeuristicEval(tempApply(gState, allMoves.front(), AI_PIECE) ,player);
					 remove(gState, allMoves.front());
					 return val;
				}
			for (Move i = allMoves.front() ; allMoves.size() > 0; allMoves.pop())
			{
				i = allMoves.front();
				//find best value of human opponenet
				int bestVal = alphaBeta(tempApply(gState, i, AI_PIECE ),what, -1,alpha,beta );	
				// std::cout<<"in ai move\n\n";
				remove(gState, i);	//need to remove the affected state for the other branches

				if (bestVal > v)
					v = bestVal;
				if (v >= (beta))
					return v;
				if (v > (alpha ))
					alpha = v;
			}
			return v;

		}
	else if (player == -1)	//is human turn, move for humans
		{
			int v = Hmax;
			std::queue<Move> allMoves = tempAction(gState);
			if (allMoves.size() == 1)
			{
				// return HeuristicEval(tempApply(gState, allMoves.front(), HUMAN_PIECE) ,player);
					 int val = HeuristicEval(tempApply(gState, allMoves.front(), HUMAN_PIECE) ,player);
					 remove(gState, allMoves.front());
					 return val;
				}
			for (Move i = allMoves.front() ; allMoves.size() > 0; allMoves.pop())
			{
				i = allMoves.front();
 				//find best value of human opponenet
				int bestVal = alphaBeta(tempApply(gState, i, HUMAN_PIECE ),what, 1 , alpha, beta);
				// std::cout<<"in human move\n\n";
				remove(gState, i);

				if (bestVal < v)
					v = bestVal;
				if (v <= (alpha ))
					return v;
				if (v < (beta ))
					beta = v;
			}
			return v;
		}
};

std::queue<Move> AIShell::tempAction(int **gState)	//Makes a temp **int to gamestate, applies action without affecting current board
{	//Need to store all possible states
	std::queue<Move> Movelist;
	for (int i = 0; i < numCols; i++){
		for (int j = 0; j < numRows; j++){
			if (gState[i][j] == NO_PIECE)
			{
				Move m = Move(i,j);
				Movelist.push(m);
				if (gravityOn)
					break;
			}
		}
	}
	return Movelist;
};

int **AIShell::tempApply (int **gState, Move m, int ply)
{
	//return a temp array that gets destroyed after all function calls in minimax and makeMove
	int ** temp = gState;
	if (ply == HUMAN_PIECE)
		temp[m.col][m.row] = HUMAN_PIECE; // -1
	else
		temp[m.col][m.row] = AI_PIECE; // 1
	return temp;
}


bool AIShell::cutOff (int ply )
{
	if (gravityOn)
	{	
		if (ply == 4)
				return true;
	}
	else 
	{
		if (ply == 3)
			return true;
	}
	return false;
}


int AIShell::winCounter( int **gState)
{	
int winner = -2;
for (int i =0 ; i < numCols; i++)
	{
		for (int j =0 ; j <numRows; j++)
		{
			// std::cout<<"i and j "<< i<<j<<"\n";
			if (validMove(i, j+1 ) && gState[i][j] != 0)	//count vertically, col-row wise
			{
				// std::cout<<"FIRST \n";
				int count = 0;
				if (!validMove(i, j+count))
						break;
				do
				{
					if (gState[i][j+count] == gState[i][j])	//ONLY INCREMENT IF YOU HAVE MATCH
						count++;
					if (count >= k)
					{
						winner = gState[i][j];
						return winner;
					}
					if (!validMove(i, j+count))
						break;
				}
				while (gState[i][j] == gState[i][j+count]);

			}
			if (validMove( i+1, j )&& gState[i][j] != 0)	//count horizontally
			{
				// std::cout<<"SECOND \n";
				int count = 0;
				if (!(validMove(i+count, j)))
						break;
				do
				{
					if (gState[i+count][j] == gState[i][j])	//ONLY INCREMENT IF YOU HAVE MATCH
						count++;
					if (count >=k)
					{
						winner = gState[i][j];
						return winner;
					}
					if (!(validMove(i+count, j)))
						break;
				}
				while (gState[i][j] == gState[i+count][j]);
				
			}
			if (validMove(  i+1, j+1 ) && gState[i][j] != 0)	//count diagonally top left to bot right
				{
					// std::cout<<"THIRD \n";
					int count = 0;
					if (!validMove(i+count, j+count))
						break;
					do
					{
						// i+count = 7
						if (gState[i+count][j+count] == gState[i][j])	//ONLY INCREMENT IF YOU HAVE MATCH
							count++;
						if (count >=k)
						{
							winner = gState[i][j];
							return winner;
						}
						if (!validMove(i+count, j+count))
							break;
					}
					while(gState[i][j] == gState[i+count][j+count]); // 8 
					
				}
			if (validMove(  i+1, j-1 ) && gState[i][j] != 0)
			{
				// std::cout<<"fourth\n";
				int count = 0;
				if (!validMove(i +count, j -count))
						break;
				do
				{
					
					if (gState[i+count][j-count] == gState[i][j])	//ONLY INCREMENT IF YOU HAVE MATCH
						count++;
					if( count >= k)
					{
						winner = gState[i][j];
						return winner;
					}
					if (!validMove(i +count, j -count))
						break;
				}
				while(gState[i][j] == gState[i + count][j - count]);
				
			}
		}
	}
	return 0;
};

int AIShell::winMove (int **gState)
{
	int val = winCounter(gState);

	if (val == HUMAN_PIECE)
		return -1;
	else if (val == AI_PIECE)
		return 1;
	return 0;
}
int AIShell::countConnect(int **gState) // Counts the number of connected lines
{
	int p1Count = 0, p2Count = 0;

	int numOffset = 8;
	//need to count col row wise
	for (int j = 0; j <numCols; j++)
	{
		for (int i = 0; i < numRows; i++)
		{
			// cout<<"Is this valid?  "<<validMove(gState,j,i)<<endl;
			int boardVal = gState[j][i];	//get current value at index
			if (boardVal != 0)		//next piece cannot be empty
			{
				for (int k = 0; k < numOffset; k++)
				{
					int nextY = j + offSet::y[k];	//proceed to check area around index
					int nextX = i + offSet::x[k];
					//make sure next move is valid and next index is same color
					if (validMove ( nextY, nextX) &&
						gState[nextY][nextX] == boardVal)	
					{
						if (boardVal == HUMAN_PIECE)
							p2Count++;
						else
							p1Count++;
						/*
	static int x[] = {0, -1, -1, -1, 0, 1, 1, 1};	//count row index around center
	static int y[] = {-1, -1, 0, 1, 1, 1, 0, -1};	//count col index around center
*/
						/*
		Checks index value around s in <^>v fashion, starting at x, ending at o
		|->	->	v
		|x	s 	v
		|o	<-	<-
	*/
					}
				}
			}

		}
	}

	// cout <<"Here a count of p1 vs p2:  "<<p1Count<< "   " << p2Count<<endl;
	return p1Count - p1Count;	//all values are positive counts
};
//win move isn't calculating win correctly
//also heuristic is incorrect




/*




	int winner = 0;
	int curVal = 0;
	if (winner == 0) 
	{
		for (int i = 0; i < numCols; ++i) 
		{
			for (int j = 0; j < numRows; ++j) 
			{
				// if the space previous is either not the same as current,
				// empty, or OOB
				// while the next thing is the same AND not OOB
				// increment contiguous count
				// if count greater than k, return the winner
				// returns on first winning sequence found
				// searches to the right and up

				if (gState[i][j] == 0) {
					if (gravityOn)
						break;// go to next column
					else
						continue;// move up
				}

				if (i - 1 < 0 || gState[i - 1][j] != gState[i][j]) { // horizontal
					int count = 1;
					while (i + count < numRows && gState[i][j] == gState[i + count][j]) {
						++count;
						if (count >= k) {
							winner = gState[i][j];
							return winner;
						}
					}
				}

				if (i - 1 < 0 || j - 1 < 0 || gState[i - 1][j - 1] != gState[i][j]) { // diagonal,
																						// (j-1<0)
																						// needed
																						// to
																						// avoid
																						// OOB
					int count = 1;
					while (i + count < numRows && j + count < numCols
							&& gState[i][j] == gState[i + count][j + count]) {
						++count;
						if (count >= k) {
							winner = gState[i][j];
							return winner;
						}
					}
				}

				if (i - 1 < 0 || j + 1 >= numCols || gState[i - 1][j + 1] != gState[i][j]) { // diagonal,
																							// (j+1>=numCols)
																							// needed
																							// to
																							// avoid
																							// OOB
					int count = 1;
					while (i + count < numRows && j - count >= 0 && gState[i][j] == gState[i + count][j - count]) {
						++count;
						if (count >= k) {
							winner = gState[i][j];
							return winner;
						}
					}
				}

				if (j - 1 < 0 || gState[i][j - 1] != gState[i][j]) { // vertical
					int count = 1;
					while (j + count < numCols && gState[i][j] == gState[i][j + count]) {
						++count;
						if (count >= k) {
							winner = gState[i][j];
							return winner;
						}
					}
				}
			}
		}
	}
	return 0;

	*/