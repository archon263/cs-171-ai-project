#include "AIShell.h"
#include <iostream>
#include <cstdlib>
// #include "algorithm"	//They have min and max
#include "stdio.h"
#include "string.h"
#include "limits.h"
#include <queue>	//used in makeMove, tempAction, and others to get a set of all possible moves to make

/*

Hello viewers, this is the previous working commit. 
Thing I know to be somewhat wrong here.
	movesLeft
	countConnect
	cutOff
	miniMax

*/
#define MAX(X,Y) (((X) < (Y)) ? (X) : (Y))
#define MIN(X,Y) (((X) > (Y)) ? (X) : (Y))

#define Hmax INT_MAX
#define Hmin INT_MIN
//try module load java/1.8.0_20 instead of 1.8_20
namespace offSet {
	static int x[] = {0, -1, -1, -1, 0, 1, 1, 1};	//count row index around center
	static int y[] = {-1, -1, 0, 1, 1, 1, 0, -1};	//count col index around center
	/*
		Checks index value around s in <^>v fashion, starting at x, ending at o
		|->	->	v
		|x	s 	v
		|o	<-	<-
	*/
}

using namespace std;

void pout (int **arrays)
{
	std::cout<<"Time to move again\n";
 	for ( int i = 6 ; i>=0; i--)
		{
			for (int j = 0 ; j<9; j++)
				std::cout<<arrays[j][i]<<" ";
			std::cout<<'\n';
		}
}

AIShell::AIShell(int numCols, int numRows, bool gravityOn, int** gameState, Move lastMove)
{
	this->deadline=0;
	this->numRows=numRows;
	this->numCols=numCols;
	this->gravityOn=gravityOn;
	this->gameState=gameState;
	this->lastMove=lastMove;
	this->moveExists = true;
	this->winFound = false;
}


AIShell::~AIShell()
{
	
	//delete the gameState variable.
	for (int i =0; i<numCols; i++){ 
		delete [] gameState[i];
	}
	delete [] gameState;

}

void remove(int **gState, Move m)
{
	gState[m.col][m.row] = 0;
}

Move AIShell::makeMove(){
	//this part should be filled in by the student to implement the AI
	//Example of a move could be: Move move(1, 2); //this will make a move at col 1, row 2
	
	int **gState = gameState; //
	int alphaVal = Hmin;

		// std::cout<<"wtf\n";

	Move m;
	int depth = 0;
	int player = 1;	//it be the computers turn now,

	//Figure out how to get all state ()
		// std::cout<<"wtf\n";

	std::queue<Move> allMoves = tempAction(gState);

	cout<<"allMoves size : "<<allMoves.size()<<endl;
	pout (gState);

	//for each state, apply action
	//This is the base state, we are starting to figure out trees of the first ply
	for (Move i = allMoves.front(); allMoves.size() > 0 ; allMoves.pop())
	{
		//first, figure out what computers next move will be, then minimax down
		i = allMoves.front();	//This is a Move
		int bestVal = miniMax(tempApply(gState,i,depth), depth, player );	//need Gstate, ply, player
		remove(gState,i);

		// cout<<"Here's our bestVal "<<bestVal<<std::endl;
		if (alphaVal < bestVal)
		{
			m.row= i.row;	//switch one move to the best move
			m.col = i.col;
			alphaVal = bestVal;
			cout<<"Is win? "<<alphaVal<<endl;
			cout<<"We got winners "<<m.row<<m.col<<endl;
		}

	}
	cout<<"We will use these values "<<m.row<<m.col<<std::endl;
	// cout<<"But first, is here a value at that location?  "<<gState[m.row][m.col]<<std::endl;
	pout (gState);

	return m;

	//loop through all available moves and call minimax on them
	//change the ply value and negate certain condition variable to notify
	//which player is currently acting

	//get best value AND index of that Move
	//return the best Move.
}

int AIShell::validMove (int ** gState, int col, int row)
{	//Check if indexes are within boundary and if gState is empty at those indexes
	if (!(row>-1 and row <numRows and col >-1 and col <numCols))
		return false;
	return true;
}



bool AIShell::movesLeft(int **gameState )
{	//used in MiniMax to see if game can be continued
	//As well as winCounter to see if no further moves can be made, 
	//Is inconclusive, or if there is a draw
	// int i= numRows-1;
	// int j=  numCols - 1;	
	
	// cout<<"Please tell me this works\n";
	// if (!gravityOn) 	//gravity not on? We'll check it all.
	// 	i = 0, j = 0;
	for ( int a = 0 ; a< numCols; a++)	//only really need to check top most
		for (int b = 0 ; b < numRows; b++)	//if gravity is on
			if (gameState[a][b] == 0)	//empty space exists
				return true;
	// cout<<"Continuing the game!\n";
	return false;
}

int AIShell::HeuristicEval ( int **gState, int player)
{ //This function will determine what will the best move to make for a player,
	//this will be used inside minimax. Will use win checker to see utility of a certain gameboard for a particular player
	//return min val for human (p1)
	//return max val for AI  (p2)
	// cout<<" I am in HeuristicEval"<<std::endl;

	int val = 0;
	int isWin = winMove(gState);
	
	if ( isWin == HUMAN_PIECE)	//if the game has no winner/ is not a draw
		return -10000;

	val += 10000 * isWin;	//if no winner, val reset back to zero.
	val += countConnect(gState);	//only count in a row, and not in a row + space + potential same player 
	// cout<<"here is val  "<<val<<std::endl;
	return val;

	
};

int AIShell::countConnect(int **gState)
{
	int p1Count = 0, p2Count = 0;

	int numOffset = 8;
	//need to count col row wise
	for (int j = 0; j <numCols; j++)
	{
		for (int i = 0; i < numRows; i++)
		{
			int boardVal = gState[j][i];	//get current value at index
			if (boardVal != 0)		//next piece cannot be empty
			{
				for (int k = 0; k < numOffset; k++)
				{
					int nextY = j + offSet::y[k];	//proceed to check area around index
					int nextX = i + offSet::x[k];
					//make sure next move is valid and next index is same color
					if (validMove (gState, nextX, nextY) &&
						gState[nextY][nextX] == boardVal)	
					{
						if (boardVal == HUMAN_PIECE)
							p1Count++;
						else
							p2Count++;
						/*
	static int x[] = {0, -1, -1, -1, 0, 1, 1, 1};	//count row index around center
	static int y[] = {-1, -1, 0, 1, 1, 1, 0, -1};	//count col index around center
*/
					}
				}
			}

		}
	}

	return p1Count - p2Count;	//all values are positive counts
};


int AIShell::miniMax (int **gState, int ply, int player )
{
	//miniMax algorithm designede for 4 ply with branching equal to width
	//or 2/3 ply with the entire gameboard as the branch
	

//determine which player is it at the terminal state to get value
//Player 1(human) gets min val/ Player 2 (AI) gets Max value
	if (movesLeft (gState) || cutOff(gState, ply) )	
		return ( player == 1) ?  
			HeuristicEval(gState,player) : -HeuristicEval (gState,player);	
	//pos for max(bot), neg for min (hum))

	int bestVal ;


	//check current board value if depth == cutoff
	if ( player == 1)	//is computer turn, figure out human's choice
		{
			bestVal = Hmin;
			std::queue<Move> allMoves = tempAction(gState);
			for (Move i = allMoves.front() ; allMoves.size() > 0; allMoves.pop())
			{
				i = allMoves.front();
				ply++;
				//find best value of human opponenet
				bestVal = MIN(bestVal, miniMax(tempApply(gState, i, HUMAN_PIECE ),ply, 2 ));
				remove(gState, i);	//need to remove the affected state for the other branches
			}
		}
	else	//is human turn, figure out computers next choice
		{
			bestVal = Hmax;
			std::queue<Move> allMoves = tempAction(gState);
			for (Move i = allMoves.front() ; allMoves.size() > 0; allMoves.pop())
			{
				i = allMoves.front();
				ply++;
				//find best value of human opponenet
				bestVal = MAX(bestVal, miniMax(tempApply(gState, i, AI_PIECE ),ply, 1 ));
				remove(gState, i);
			}
		}
	return bestVal;
};

std::queue<Move> AIShell::tempAction(int **gState)	//Makes a temp **int to gamestate, applies action without affecting current board
{	//Need to store all possible states
	std::queue<Move> Movelist;


	// for (int i = numCols - 1; i >=0; i--){
	// 	for (int j = numRows - 1; j >=0; j--){
	for (int i = 0; i < numCols; i++){
		for (int j = 0; j < numRows; j++){
			if (gState[i][j] == NO_PIECE){
				Move m = Move(i,j);
				Movelist.push(m);
				if (gravityOn)
					break;
			}
		}
	}
	return Movelist;
};
int **AIShell::tempApply (int **gState, Move m, int ply)
{
	//return a temp array that gets destroyed after all function calls in minimax and makeMove
	int ** temp = gState;
	if (ply == HUMAN_PIECE)
		temp[m.col][m.row] = HUMAN_PIECE;
	else
		temp[m.col][m.row] = AI_PIECE;
	return temp;
}


bool AIShell::cutOff (int **gState, int ply )
{
	if (gravityOn)
	{	
		if (ply == 4)
			{
				cout<<"gravity is on\n";
				return true;
			}
	}
	else 
	{
		if (ply == 3)
			{
				cout<<"gravity is off\n";
				return true;
			}
	}
	return false;

}


int AIShell::winCounter( int **gState)
{	//Return 1 or -1, if player one or player two has won
	//0 if no one is a winner
	// return 
	int winner = -2, count = 1;
	if (winner == -2)
	{
		for ( int i = 0; i< numCols; i++)
		{
			for (int j = 0; j < numRows; j++)
			{
				if (gState[i][j] == NO_PIECE)
					if (gravityOn )
						break;	//go to next column
					else
						continue;

				if (validMove(gState, i, j+1) )	//count vertically, col-row wise
				{
					count = 1;
					while (gState[i][j] == gState[i][j+count])
					{
						++count;
						if (count >= k)
						{
							winner = gState[i][j];
							break;
						}
					}

				}
				if (validMove(gState, i+1, j ))	//count horizontally
				{
					count = 1;
					while (gState[i][j] == gState[i+count][j])
					{
						++count;
						if (count >=k)
						{
							winner = gState[i][j];
							break;
						}
					}
				}
				if (validMove(gState, i+1, j+1))	//count diagonally top left to bot right
				{
					count = 1;
					while(gState[i][j] == gState[i+count][j+count])
					{
						++count;
						if (count >=k)
						{
							winner = gState[i][j];
							break;
						}
					}
				}
				if (validMove(gState, i+1, j-1))
				{
					count = 1;
					while(gState[i][j] == gState[i + count][j - count])
					{
						++count;
						if( count >= k)
						{
							winner = gState[i][j];
							break;
						}
					}
				}
			}	//Inner for loop

			winner = movesLeft( gState) ? 0: -3;	//there are still moves vs tie
		}		//outer for loop
	}//if loop end
	
	// if (winner != -3 && winner != -2)	//update to let us know when to quit
	// 	winFound = true;
	// lastMove = Move(i,j);	//hopefully gets the last winning index
	return winner;
};

int AIShell::winMove (int **gState)
{
	int val = winCounter(gState);

	if (val == HUMAN_PIECE)
		return -1;
	else if (val == AI_PIECE)
		return 1;
	return 0;
}
