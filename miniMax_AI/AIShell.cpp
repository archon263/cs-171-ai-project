#include "AIShell.h"
#include <iostream>
#include <cstdlib>
// #include "algorithm"	//They have min and max
#include "stdio.h"
#include "string.h"
#include "limits.h"
#include <queue>	//used in makeMove, tempAction, and others to get a set of all possible moves to make


/*
	Hello viewers, this is the current working commit. 
	Thing I know to be somewhat wrong here.
	
	Heurisitc is almost working,

	Minimax

 */

#define MAX(X,Y) (((X) > (Y)) ? (X) : (Y))
#define MIN(X,Y) (((X) < (Y)) ? (X) : (Y))

#define Hmax INT_MAX
#define Hmin INT_MIN
//try module load java/1.8.0_20 instead of 1.8_20
namespace offSet {
	static int x[] = {0, -1, -1, -1, 0, 1, 1, 1};	//count row index around center
	static int y[] = {-1, -1, 0, 1, 1, 1, 0, -1};	//count col index around center
	/*
		Checks index value around s in <^>v fashion, starting at x, ending at o
		|->	->	v
		|x	s 	v
		|o	<-	<-
	*/
}

using namespace std;

void AIShell::pout (int **arrays)
{
	std::cout<<"Time to move again\n";
 	for ( int i = numRows-1 ; i>=0; i--)
		{
			for (int j = 0 ; j<numCols; j++)
			{
				char ply ;
				int what = arrays[j][i];
				if (what == -1)
					ply = 'x';
				else if (what == 0)
					ply = '-';
				else
					ply = 'o';
				std::cout<<ply<<" ";
			}
			std::cout<<'\n';
		}
	// for (int i = 9-1 ; i>=0; i--)
	// {
	// 	for (int j = 7-1 ; j >= 0; j--)
	// 		std::cout<<arrays[i][j]<<" ";
	// 	std::cout<<'\n';
	// }
	// for (int i = 0 ; i < numCols; i++)
	// 	{
	// 		for (int j = 0 ; j <numRows; j++)
	// 			std::cout<<arrays[i][j]<<" ";
	// 		std::cout<<"\n";
	// 	}

}

AIShell::AIShell(int numCols, int numRows, bool gravityOn, int** gameState, Move lastMove)
{
	this->deadline=0;
	this->numRows=numRows;
	this->numCols=numCols;
	this->gravityOn=gravityOn;
	this->gameState=gameState;
	this->lastMove=lastMove;
	this->moveExists = true;
	this->winFound = false;
}


AIShell::~AIShell()
{
	
	//delete the gameState variable.
	for (int i =0; i<numCols; i++){ 
		delete [] gameState[i];
	}
	delete [] gameState;

}

void remove(int **gState, Move m)
{
	gState[m.col][m.row] = 0;
}

Move AIShell::makeMove(){
	//this part should be filled in by the student to implement the AI
	//Example of a move could be: Move move(1, 2); //this will make a move at col 1, row 2
	
	int **gState = gameState; 
	int alphaVal = Hmin;	//-2157something

	Move m = Move();
	int depth = 1;	//start at 1 for the ply beneath zero.
	

	//Figure out how to get all state ()
		// std::cout<<"wtf\n";

	std::queue<Move> allMoves = tempAction(gState);

	// cout<<"allMoves size : "<<allMoves.size()<<endl;
	// pout (gState);
	int bestVal = Hmin;
	//for each state, apply action
	//This is the base state, we are starting to figure out trees of the first ply
	for (Move i = allMoves.front(); allMoves.size() > 0 ; allMoves.pop())
	{
		//first, figure out what computers next move will be, then minimax down
		i = allMoves.front();	//This is a Move
		//added max here
		bestVal = MAX(bestVal,miniMax(tempApply(gState,i,AI_PIECE), depth, HUMAN_PIECE ));	//do ai move now
		remove(gState,i);

		// cout<<"Here's our bestVal "<<bestVal<<std::endl;
		// cout << "\nBestVal for " << i.col << i.row << "is " << bestVal << endl;

		if (bestVal > alphaVal)
		{
			m.row= i.row;	//switch one move to the best move
			m.col = i.col;
			alphaVal = bestVal;
			// cout<<"We got winners "<<m.col<<m.row<<endl;
		}

	}
	// cout<<"We will use these values "<<m.col<<m.row<<std::endl;
	return m;
}

int AIShell::validMove ( int col, int row)
{	//Check if indexes are within boundary and if gState is empty at those indexes
	if (!(row>-1 and row <numRows and col >-1 and col <numCols))
		return false;
	return true;
}
bool AIShell::movesLeft(int **gameState )
{	//used in MiniMax to see if game can be continued
	//As well as winCounter to see if no further moves can be made, 
	
	for ( int a = 0 ; a< numCols; a++)	
		for (int b = 0 ; b < numRows; b++)	
			if (gameState[a][b] == 0)	//empty space exists
				return true;
	// cout<<"Continuing the game!\n";
	return false;
}
int AIShell::HeuristicEval ( int **gState, int player)
{ 
/*	This function will determine what will the best move to make for a player,
	this will be used inside minimax. Will use win checker to see utility of a certain gameboard for a particular player
	return min val for human (p1)
	return max val for AI  (p2)
	cout<<" I am in HeuristicEval"<<std::endl;
*/
	int val = 0;
	val += countAllrows(gState);
	return val;
 };
void countHelp (int cVal ,int &AI, int &HUM, int &EMP)
{
	//pass by reference function to see how many connected rows
	//for empty, human, and ai given game piece.
	if (cVal == 0)	//if empty place, everyone gets a plus 1
	{
		AI++; HUM++; EMP++;
	}
	else if (cVal == 1)
	{
		AI += 1 + EMP;	//count is now 1 plus number of empty rows
		EMP = 0;	//reset to 0
		HUM = 0;
	}
	else	//if value is human, change it accordingly
	{
		HUM += 1 + EMP;
		EMP = 0;
		AI = 0;
	}

}

void countHelpPlayer( int ai, int hum, int kval, int &p1, int &p2)
{	//two args with pass by reference that will make function less cluttered
	//Increment p1 and p2 count as necessary.
	if (ai>=kval-2)
		p1 += 5;
	if (hum >= kval-2)
		p2 += 5;
	if (ai >= kval)
		p1 += 10;
	else if (hum >= kval)
		p2 += 10;
}

int AIShell::countAllrows(int **gState)
{	
	//Counts how many possible win locations in a row/col/diag
	//for a particular player
	int p1Count = 0, p2Count = 0;	//p1 == AI , p2 = HUm
	int A=0, H= 0, E = 0;	//AI piece, human piece, empty piece
	//count horizontal for both   [i+1] [j]
	for (int i = 0; i < numCols; i++)	
	{
		// std::cout<<"in the horizin\n\n";
		for (int j = 0; j < numRows; j++)
		{
			int count = 0;
			do
			{
				if (!validMove(i+count, j) )	//make sure it's within boundary
					break;	
				int current = gState[i +count][j];
				countHelp(current, A,H,E);	//returns back how many in a row for Human, AI, empty
				countHelpPlayer(A,H, k, p1Count, p2Count);	//modify by reference if rows meets win req
				count++;	//increment count 

			} while(validMove(i+count, j));	//check if it is still alright
		}
	}
	//count vertical for both	[i][j+1]
	for (int i = 0; i < numCols; i++)
	{
		for (int j = 0; j < numRows; j++)
		{
			int count = 0;
			do
			{
				if (!validMove(i, j+count) )	 
					break;	
				int current = gState[i][j+count];
				countHelp(current, A,H,E);	
				countHelpPlayer(A,H, k, p1Count, p2Count);
				count++;	//increment count 

			} while(validMove(i, j+count));	//check if it is still alright			
		}
	}
	//count Upleft down right for both	[i+1][j+1]
	for (int i = 0; i < numCols; i++)
	{
		for (int j = 0; j < numRows; j++)
		{
			int count = 0;
			do
			{
				if (!validMove(i+count, j+count) )	 
					break;	
				int current = gState[i+count][j+count];
				countHelp(current, A,H,E);	
				countHelpPlayer(A,H, k, p1Count, p2Count);
				count++;	//increment count 

			} while(validMove(i+count, j+count));	//check if it is still alright		
		}
	}
	//count downleft to up right  for both [i+1][j-1]
	for (int i = 0; i < numCols; i++)
	{
		for (int j = 0; j < numRows; j++)
		{
			int count = 0;
			do
			{
				if (!validMove(i+count, j-count) )	 //check if it even within boundaries
					break;	
				int current = gState[i+count][j - count];
				countHelp(current, A,H,E);	
				countHelpPlayer(A,H, k, p1Count, p2Count);
				count++;	//increment count 

			} while(validMove(i+count, j-count));	//check if it is still alright		
		}
	}

	// std::cout<<"p1 and p2   :"<<p1Count<<" "<<p2Count<<"\n";
 	return p1Count - p2Count;
}



int AIShell::miniMax (int **gState, int ply, int player )
{
	//miniMax algorithm designede for 4 ply with branching equal to width
	//or 2/3 ply with the entire gameboard as the branch

//determine which player is it at the terminal state to get value
//Player 1(Computer 1) gets max val/ Player 2 (Human -1) gets min value
	int what = ply;
	int win = winMove(gState);
	// cout << " win value " <<win<< endl;

	if (win != 0 )
	{
		// pout(gState);
		// cout<<"it exists!!!! "<<win<<"\n\n";
		if (win == -1)
			return -10000;
		else
			return 	10000;
	}

	if (cutOff(what) )	//cutoff now works
		return HeuristicEval(gState,player);
	int bestVal = 0;

	what++;	//this increments ply
	//check current board value if depth == cutoff
	if ( player == 1)	//is computer turn,move for computer
		{
			bestVal = Hmin;
			std::queue<Move> allMoves = tempAction(gState);
			for (Move i = allMoves.front() ; allMoves.size() > 0; allMoves.pop())
			{
				i = allMoves.front();
				//find best value of human opponenet
				bestVal = MAX(bestVal, miniMax(tempApply(gState, i, AI_PIECE ),what, -1 ))  ;	
				// std::cout<<"in ai move\n\n";
				remove(gState, i);	//need to remove the affected state for the other branches
			}
				// return bestVal;

		}
	else if (player == -1)	//is human turn, move for humans
		{
			bestVal = Hmax;
			std::queue<Move> allMoves = tempAction(gState);
			for (Move i = allMoves.front() ; allMoves.size() > 0; allMoves.pop())
			{
				i = allMoves.front();
 				//find best value of human opponenet
				bestVal = MIN(bestVal, miniMax(tempApply(gState, i, HUMAN_PIECE ),what, 1 )  );
				// std::cout<<"in human move\n\n";
				remove(gState, i);
			}
				// return bestVal;

		}
	return bestVal;
};

std::queue<Move> AIShell::tempAction(int **gState)	//Makes a temp **int to gamestate, applies action without affecting current board
{	//Need to store all possible states
	std::queue<Move> Movelist;
	for (int i = 0; i < numCols; i++){
		for (int j = 0; j < numRows; j++){
			if (gState[i][j] == NO_PIECE)
			{
				Move m = Move(i,j);
				Movelist.push(m);
				if (gravityOn)
					break;
			}
		}
	}
	return Movelist;
};

int **AIShell::tempApply (int **gState, Move m, int ply)
{
	//return a temp array that gets destroyed after all function calls in minimax and makeMove
	int ** temp = gState;
	if (ply == HUMAN_PIECE)
		temp[m.col][m.row] = HUMAN_PIECE; // -1
	else
		temp[m.col][m.row] = AI_PIECE; // 1
	return temp;
}


bool AIShell::cutOff (int ply )
{
	if (gravityOn)
	{	
		if (ply == 4)
				return true;
	}
	else 
	{
		if (ply == 3)
			return true;
	}
	return false;
}


int AIShell::winCounter( int **gState)
{	
int winner = -2;
for (int i =0 ; i < numCols; i++)
	{
		for (int j =0 ; j <numRows; j++)
		{
			// std::cout<<"i and j "<< i<<j<<"\n";
			if (validMove(i, j+1 ) && gState[i][j] != 0)	//count vertically, col-row wise
			{
				// std::cout<<"FIRST \n";
				int count = 0;
				if (!validMove(i, j+count))
						break;
				do
				{
					if (gState[i][j+count] == gState[i][j])	//ONLY INCREMENT IF YOU HAVE MATCH
						count++;
					if (count >= k)
					{
						winner = gState[i][j];
						return winner;
					}
					if (!validMove(i, j+count))
						break;
				}
				while (gState[i][j] == gState[i][j+count]);

			}
			if (validMove( i+1, j )&& gState[i][j] != 0)	//count horizontally
			{
				// std::cout<<"SECOND \n";
				int count = 0;
				if (!(validMove(i+count, j)))
						break;
				do
				{
					if (gState[i+count][j] == gState[i][j])	//ONLY INCREMENT IF YOU HAVE MATCH
						count++;
					if (count >=k)
					{
						winner = gState[i][j];
						return winner;
					}
					if (!(validMove(i+count, j)))
						break;
				}
				while (gState[i][j] == gState[i+count][j]);
				
			}
			if (validMove(  i+1, j+1 ) && gState[i][j] != 0)	//count diagonally top left to bot right
				{
					// std::cout<<"THIRD \n";
					int count = 0;
					if (!validMove(i+count, j+count))
						break;
					do
					{
						// i+count = 7
						if (gState[i+count][j+count] == gState[i][j])	//ONLY INCREMENT IF YOU HAVE MATCH
							count++;
						if (count >=k)
						{
							winner = gState[i][j];
							return winner;
						}
						if (!validMove(i+count, j+count))
							break;
					}
					while(gState[i][j] == gState[i+count][j+count]); // 8 
					
				}
			if (validMove(  i+1, j-1 ) && gState[i][j] != 0)
			{
				// std::cout<<"fourth\n";
				int count = 0;
				if (!validMove(i +count, j -count))
						break;
				do
				{
					
					if (gState[i+count][j-count] == gState[i][j])	//ONLY INCREMENT IF YOU HAVE MATCH
						count++;
					if( count >= k)
					{
						winner = gState[i][j];
						return winner;
					}
					if (!validMove(i +count, j -count))
						break;
				}
				while(gState[i][j] == gState[i + count][j - count]);
				
			}
		}
	}

	return movesLeft(gState) ? 0: -3;

};

int AIShell::winMove (int **gState)
{
	int val = winCounter(gState);

	if (val == HUMAN_PIECE)
		return -1;
	else if (val == AI_PIECE)
		return 1;
	return 0;
}
int AIShell::countConnect(int **gState) // Counts the number of connected lines
{
	int p1Count = 0, p2Count = 0;

	int numOffset = 8;
	//need to count col row wise
	for (int j = 0; j <numCols; j++)
	{
		for (int i = 0; i < numRows; i++)
		{
			// cout<<"Is this valid?  "<<validMove(gState,j,i)<<endl;
			int boardVal = gState[j][i];	//get current value at index
			if (boardVal != 0)		//next piece cannot be empty
			{
				for (int k = 0; k < numOffset; k++)
				{
					int nextY = j + offSet::y[k];	//proceed to check area around index
					int nextX = i + offSet::x[k];
					//make sure next move is valid and next index is same color
					if (validMove ( nextY, nextX) &&
						gState[nextY][nextX] == boardVal)	
					{
						if (boardVal == HUMAN_PIECE)
							p2Count++;
						else
							p1Count++;
						/*
	static int x[] = {0, -1, -1, -1, 0, 1, 1, 1};	//count row index around center
	static int y[] = {-1, -1, 0, 1, 1, 1, 0, -1};	//count col index around center
*/
						/*
		Checks index value around s in <^>v fashion, starting at x, ending at o
		|->	->	v
		|x	s 	v
		|o	<-	<-
	*/
					}
				}
			}

		}
	}

	// cout <<"Here a count of p1 vs p2:  "<<p1Count<< "   " << p2Count<<endl;
	return p1Count - p1Count;	//all values are positive counts
};
//win move isn't calculating win correctly
//also heuristic is incorrect