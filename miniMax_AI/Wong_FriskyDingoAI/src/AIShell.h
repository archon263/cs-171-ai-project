#ifndef AISHELL_H
#define AISHELL_H

#pragma once
#include "Move.h"
#include <queue>
#include <time.h>

// A new AIShell will be created for every move request.
class AIShell{

public:
	//these represent the values for each piece type.
	static const int AI_PIECE=1;
	static const int HUMAN_PIECE = -1;
	static const int NO_PIECE=0;


private:
	//Do not alter the values of numRows or numcols.
	//they are used for deallocating the gameState variable.
	int numRows; //the total number of rows in the game state.
	int numCols; //the total number of columns in the game state.
	int **gameState; //a pointer to a two-dimensional array representing the game state.
	bool gravityOn; //this will be true if gravity is turned on. It will be false if gravity is turned off.
	Move lastMove; //this is the move made last by your opponent. 
					//If your opponent has not made a move yet (you move first) 
					//then this move will hold the value (-1, -1) instead.

public:
	int deadline; //this is how many milliseconds the AI has to make move.
	int k;        // k is the number of gState a player must get in 
	//a row/column/diagonal to win the game. IE in connect 4, this variable would be 4
	

	AIShell(int numCols, int numRows, bool gravityOn, int** gameState, Move lastMove);
	~AIShell();
	Move makeMove();

	
	bool movesLeft(int **gameState);
	
	int validMove ( int col, int row);
	std::queue<Move> tempAction(int **gState);	//Got to have someway to capture all possible moves
	int **tempApply (int **gState, Move m, int ply);	//temporarily apply state of change


	int HeuristicEval ( int **gState, int ply);	//calculate utility, return 
	//A pass by reference function to calculate and mod values within HeuristicEval	
	int countConnect(int **gState);
	//int **countConnect(int **gState, int rowC, int colC,
	 //int diagUp, int diagDown);	//used with heuristic to find utility of current game state
	int countAllrows(int **gState);
<<<<<<< HEAD:miniMax_AI/Wong_FriskyDingoAI/src/AIShell.h
	int miniMax (int **gState, int ply, int player );	//will decide which move to take based on heurisitcs
=======

	int IDS_search(int ** gState, clock_t currentTime, int timeLimit);
	int alphaBeta (int **gState, int ply, int player ,int alpha,
	 int beta, clock_t cTime, int timeLimit);	//will decide which move to take based on heurisitcs

>>>>>>> d1de000baa1c981c162ab721cd183e7fe189b33a:IDS_AI/AIShell.h
	bool cutOff (int ply); 		//will check current win state
	
	int winCounter ( int **gState);	//find if there is a winner, only called in winMove
	int winMove (int **gState);		//determine what winner they are

	void pout( int **arrays);
};

#endif //AISHELL_H
